package com.example.tp2_bdd;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+"("
            + _ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_NAME + " TEXT,"
            + COLUMN_WINE_REGION + " TEXT,"
            + COLUMN_LOC + " TEXT,"
            + COLUMN_CLIMATE +" TEXT,"
            + COLUMN_PLANTED_AREA +" TEXT);";

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_TABLE); // on créé la table avec les infos dispo plus basses (dans le même ordre au cas où)
        populate(db); // on appelle la fonction plus basse avec les infos du vin
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.execSQL("DROP TABLE IF EXISTS'" + TABLE_NAME +"'");
        onCreate(db);
    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine, SQLiteDatabase db)
    {
        //PARTIE 2 cf cours 3 diapo 14
        ContentValues valeurCellier = new ContentValues();
        valeurCellier.put(COLUMN_NAME, wine.getTitle());
        valeurCellier.put(COLUMN_WINE_REGION, wine.getRegion());
        valeurCellier.put(COLUMN_LOC, wine.getLocalization());
        valeurCellier.put(COLUMN_CLIMATE, wine.getClimate());
        valeurCellier.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());

        // Inserting Row
        long rowID = 0;
	    db.insert(TABLE_NAME, null, valeurCellier);
        //db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues valeurCellier = new ContentValues();
        valeurCellier.put(COLUMN_NAME, wine.getTitle());
        valeurCellier.put(COLUMN_WINE_REGION, wine.getRegion());
        valeurCellier.put(COLUMN_LOC, wine.getLocalization());
        valeurCellier.put(COLUMN_CLIMATE, wine.getClimate());
        valeurCellier.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        // updating row
	    return db.update(TABLE_NAME, valeurCellier, _ID+" = ?", new String []{Long.toString(wine.getId())});

    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        //PARTIE 2 : la commande ci dessous renvoie plus haut dans le fichier dans la creation de table pour récupérer les informations nécessaires.
        Cursor cursor =  db.query(WineDbHelper.TABLE_NAME,
                new String[] {_ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC,COLUMN_CLIMATE,COLUMN_PLANTED_AREA},
                null,null,null,null,null, null);

        // call db.query()
        if (cursor != null)
        {
            cursor.moveToFirst();
        }

        return cursor;
    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();
        Wine wine = cursorToWine(cursor);

         db.delete(TABLE_NAME,"rowid=?",new String[]{String.valueOf(wine.getId())} );


     }

     public void populate(SQLiteDatabase db) {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"), db);
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"), db);
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"), db);
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"), db);
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"), db);
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"), db);
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"), db);
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"), db);
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"), db);
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"), db);
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"), db);

        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);

        //db.close();
    }


    public static Wine cursorToWine(Cursor cursor)
    {
        try
        {
            int idIndex = cursor.getColumnIndexOrThrow(_ID);
            long id = cursor.getLong(idIndex);

            int idTitle = cursor.getColumnIndexOrThrow(COLUMN_NAME);
            String title = cursor.getString(idTitle);

            int idRegion = cursor.getColumnIndexOrThrow(COLUMN_WINE_REGION);
            String region = cursor.getString(idRegion);

            int idLocalization = cursor.getColumnIndexOrThrow(COLUMN_LOC);
            String localization = cursor.getString(idLocalization);

            int idClimate = cursor.getColumnIndexOrThrow(COLUMN_CLIMATE);
            String climate = cursor.getString(idClimate);

            int idPlantedArea = cursor.getColumnIndexOrThrow(COLUMN_PLANTED_AREA);
            String plantedArea = cursor.getString(idPlantedArea);
            return new Wine(id, title, region, localization, climate, plantedArea);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }

    }

}
