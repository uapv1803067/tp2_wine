package com.example.tp2_bdd;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.w3c.dom.Text;

// PARTIE 3
public class WineActivity extends AppCompatActivity
{
    private Wine alerteVin;
    private boolean nouveauVin;
    WineDbHelper wineDbHelper;

    @Override
    protected void onCreate(Bundle saveInstanceState)
    {
//_________________________________________________________________________________________
        final AlertDialog.Builder alerteChamp = new AlertDialog.Builder(this);
        alerteChamp.setTitle("Erreur");
        alerteChamp.setMessage("Les champs ne doivent pas être vide.");
//_________________________________________________________________________________________
        // PARTIE 6
// ATTENTION : Faire la condition IF pour l'ajout de vins similaire avec le cursor sur bdd
        new AlertDialog.Builder(this).setTitle("Erreur Sauvegarde").setMessage("Un vin similaire a déjà été ajouté dans la base de données.").show();

//_________________________________________________________________________________________
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();
        final Wine wine = intent.getParcelableExtra("wine");

        final WineDbHelper db = new WineDbHelper(this);


        final TextView wineName = findViewById(R.id.wineName);
        wineName.setText(wine.getTitle());

        final EditText editWineRegion = findViewById(R.id.editWineRegion);
        editWineRegion.setText(wine.getRegion());
        final EditText editLoc = findViewById(R.id.editLoc);
        editLoc.setText(wine.getLocalization());
        final EditText editClimate = findViewById(R.id.editClimate);
        editClimate.setText(wine.getClimate());
        final EditText editPlantedArea = findViewById(R.id.editPlantedArea);
        editPlantedArea.setText(wine.getPlantedArea());

        Button bSave = findViewById(R.id.button);
        bSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(( (TextView) findViewById(R.id.wineName) ).getText().toString().isEmpty())
                {
                    AlertDialog.Builder popup = new AlertDialog.Builder(WineActivity.this);
                    popup.setTitle("Sauvegarde impossible");
                    popup.setMessage("Veuillez remplir les champs.");
                    popup.show();
                    return;
                }

                Wine newWine = new Wine
                        (
                                wine.getId(),
                                wineName.getText().toString(),
                                editWineRegion.getText().toString(),
                                editLoc.getText().toString(),
                                editClimate.getText().toString(),
                                editPlantedArea.getText().toString()
                        );
                if (wine.getId() < 0)
                    db.addWine(newWine, db.getWritableDatabase());
                else
                    db.updateWine(newWine);

                finish();
            }

        });
    }
}