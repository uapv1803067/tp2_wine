package com.example.tp2_bdd;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;


public class MainActivity extends AppCompatActivity
{
    ListView listView;
    Cursor cursor;
    WineDbHelper wineDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // PARTIE 3
        wineDbHelper = new WineDbHelper(this); //créé lactivité
        updateList();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);

                Intent infoVin = new Intent(MainActivity.this, WineActivity.class);
                Parcelable parcelable = WineDbHelper.cursorToWine(cursor);
                infoVin.putExtra("wine", parcelable);
                startActivity(infoVin);
            }
        });

        //code de base
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent infoVin = new Intent(MainActivity.this, WineActivity.class);
                Parcelable parcelable = new Wine(-1, "nouveau vin", "", "", "", "");
                infoVin.putExtra("wine", parcelable);
                startActivity(infoVin);
                updateList();
            }
        });


        registerForContextMenu(listView);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        if (v.getId() == R.id.listWine){
            AdapterView.AdapterContextMenuInfo info =(AdapterView.AdapterContextMenuInfo)menuInfo;
            MenuItem menu1=menu.add("Supprimer");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        int position = info.position;
        switch (menuItem.getItemId())
        {
            case 0:
                Cursor cursor = wineDbHelper.fetchAllWines();
                cursor.moveToPosition(position);
                wineDbHelper.deleteWine(cursor);
                updateList();
                break;
            default:
                break;
        }
        return true;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // PARTIE 2
    public void updateList()
    {
        cursor = wineDbHelper.fetchAllWines();

        String[] valeurVins = new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION};

        int[] to = new int[]{R.id.nomVin, R.id.regionVin};

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.affichage_main,cursor, valeurVins,to, 0);

        listView = (ListView)findViewById(R.id.listWine); //va dans content main
        listView.setAdapter(adapter);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        updateList();
    }
}